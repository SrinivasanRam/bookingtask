/// <reference types="Cypress" />
import LoginPage from '../../support/pageObjects/loginPage'
import HomePage from '../../support/pageObjects/homePage'
import EditProfile from '../../support/pageObjects/userEditPage'
import CommonFuns from '../../support/pageObjects/commonFuns'
describe("bookingKit test suit",function(){
    before(function(){
        cy.fixture('properties').then(function(data){
            this.data=data;
        })
    });
    const loginPage=new LoginPage()
    const homePage=new HomePage()
    const editProfile=new EditProfile()
    const commonFuns=new CommonFuns()
    it("Test case 1: Edit the profil and change language preference",function(){
        
        cy.visit(this.data.url)
        loginPage.getUserName().type(this.data.username)
        loginPage.getPassword().type(this.data.password)
        loginPage.loginSubmit().click()
        homePage.userInfo().should('contain',this.data.accountID)
        homePage.userInfo().trigger('show').click()
        homePage.clickEditProfile()
        editProfile.pageHeader().should('be.visible')
        editProfile.selectLanguageRadioButton().check('de').should('be.checked')
        editProfile.saveButton().click()
        editProfile.changeNofiticationMsg().should('be.visible')
        editProfile.pageHeader().should('contain','Nutzeraccount')
        commonFuns.clickMenuToggler().click()
        commonFuns.clickDashBoardTab().click()
        homePage.userInfo().trigger('show').click()
        homePage.clickLogout()
        
       
    
    })
})