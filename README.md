# BookingTask

**BookingKit Automation work**
- Tool used to Automate : Cypress version 4.9.0

- Programming Langauge : JavaScript / Nodejs

- Type of Framework : For this project used page object model and data driven framework

- POM: As per the page object model, I have maintained a class for every web page. Each web page has a separate class which holds functionalities and members of that web page

- Packages: Cypress has inbuild packages like fixtures where we can use for test data,support to maintain our page objects,integrations to have our spec files

- Test Report: I have used test execution on  Cypress dash board and Mochawesome report. Just run the html file to see the report. 

- Package.json file: This will help to make it easy for others to manage and install the npm.

- Note: Pass and Fail report is available in mochawesome-report folder 



**How to run the script:**
Pre-requisite - Need cypress and nodejs installed

1. Go to terminal -> navigate to the path bookingtask folder and paste node_modules\.bin\cypress open and click enter
2. A test runner will open then you can find all your test folders on right side ,just click bookingKit.spec.js
3. A new chrome window opens and test run on it


**Another way to run:**
1. open package.json file and right click and run on this line "specificTest": "npm run mochaReport -- --spec \"cypress/integration/examples/bookingKit.spec.js\"", 

I am ready to show a demo for better understanding.
